
#install Ubuntu dependencies

sudo apt-get update
sudo apt-get upgrade

#install environment dependencies
echo -e "\n--- Install environment dependencies --------------------------------\n"
sudo apt-get -y install git nginx php7.0-fpm php7.0-cli php7.0-gd curl php7.0-curl php7.0-intl php-pear php7.0-mcrypt php-memcache


#HTTP_request2 - Phalcon doesn't have an own http_client lib
echo -e "\n--- Http2 Request Phalcon doesn't have an own http_client lib ------------------------------\n"
pear install HTTP_Request2
sudo apt-get update
sudo /etc/init.d/nginx restart

#Packages for phalcon instalation
echo -e "\n--- Packages for phalcon instalation --------------------------------------------\n"	
sudo apt-get install -y gcc make re2c libpcre3-dev php7.0-dev build-essential php7.0-zip

#Install composer
echo -e "\n--- Install composer -------------------------------------------------------------------\n"
sudo curl -sS http://getcomposer.org/installer | php
sudo mv composer.phar /usr/local/bin/composer

#Install phalconphp with php7
echo -e "\n--- Phalconphp with php7 -----------------------------------------------------------------\n"
curl -s https://packagecloud.io/install/repositories/phalcon/stable/script.deb.sh | sudo bash
sudo apt-get install php7.0-phalcon

#Install phalcon dev tool
echo -e "\n--- Phalcon dev tool ------------------------------------------------------------\n" 
composer require "phalcon/devtools" -d /usr/local/bin/
sudo ln -s /usr/local/bin/vendor/phalcon/devtools/phalcon.php /usr/bin/phalcon

#Nginx Conf
echo -e "\n--- Configuring Nginx ---------------------------------------------------\n"
sudo cp /vagrant/src/default /etc/nginx/sites-available
sudo cp /vagrant/src/default /etc/nginx/sites-enabled

#Create logs and cache folders
echo -e "\n--- Creating Folders ---------------------\n"
sudo mkdir /vagrant/var
sudo mkdir /vagrant/var/logs

#Write access to all
echo -e "\n--- Write access to var folders --------------------------------\n"
sudo chmod -R 777 /vagrant/var/

#phalcon create-project app
#END
echo -e "\n--- Restarting Services --------------------------------\n"
sudo /etc/init.d/nginx restart
sudo /etc/init.d/php7.0-fpm restart
