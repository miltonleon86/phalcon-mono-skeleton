<?php

/**
 * Created by PhpStorm.
 * User: miltonleon
 * Date: 21.03.17
 * Time: 18:02
 */

use Phalcon\Mvc\Model;

class TestModel extends Model
{

	/**
	 * @return string
	 */
	public static function test()
	{
		return 'i am a model';
	}
}