<?php

class IndexController extends ControllerBase
{
	/**
	 * Index.
	 */
	public function indexAction()
	{
		$this->view->disable();
		echo 'Hello Index';
    }

	/**
	 * Second.
	 */
	public function secondAction()
	{
		$this->view->disable();
		echo 'Second Action';
	}

	/**
	 * Show View.
	 */
	public function thirdAction()
	{
		$this->view->setVar('model', TestModel::test());
	}
}

