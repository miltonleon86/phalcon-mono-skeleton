Sample Monolithic Phalcon Architecture.

1. Intall Virtual Box
2. Install Vagrant
3. Go to root phalconmono
4. vagrant up

add to /etc/hosts
192.168.33.11   lp.local


(Index)
http://lp.local/

(Index but other Action)
http://lp.local/index/second

(Index Controller testing view and model)
http://lp.local/index/third


(Other Controller)
http://lp.local/second/

(other method for second controller)
http://lp.local/second/second
