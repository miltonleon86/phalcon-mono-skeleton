<?php

$loader = new \Phalcon\Loader();

/**
 * We're a registering a set of directories taken from the configuration file
 */
$loader->registerDirs(
    [
        $config->application->controllersDir,
        $config->application->modelsDir,
        $config->application->lpConfigDir
    ]
)->register();

$loader->registerNamespaces(
    [
        "app\\config\\lpConfig" => $config->application->lpConfigDir
    ]
)->register();
