<?php

/**
 * Created by PhpStorm.
 * User: miltonleon
 * Date: 21.03.17
 * Time: 18:19
 */
class SecondController extends ControllerBase
{
	/**
	 * Index.
	 */
	public function indexAction()
	{
		$this->view->disable();
		echo 'Hello Index second Controller';
	}

	/**
	 * Index.
	 */
	public function secondAction()
	{
		$this->view->disable();
		echo 'Hello second from second controller';
	}
}